import axios from 'axios'
import React from 'react'

export default function useRequest ({ url, method, body, onSuccess }) {
    const [errors, setErrors] = React.useState(null)
    const BASE_URL = "http://localhost:5000";

    const doRequest = async () => {
        try {
            setErrors(null)
            const response = await axios[method](`${BASE_URL}${url}`, body)

            if(onSuccess) {
                onSuccess(response.data)
            }
            return response.data
        } catch (error) {
            setErrors(
                <div>
                    <h1>Ooops...</h1>
                    {error.response}
                </div>
            )
        }
    }

    return { doRequest, errors }
}