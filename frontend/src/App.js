import { BrowserRouter, Routes, Route } from 'react-router-dom'
// import logo from './logo.svg';
import Index from './Pages/Index'
import Question from './Pages/Question'
import "./styles/main.scss";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Index />} />
        <Route path='/question/:slug' element={<Question />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
