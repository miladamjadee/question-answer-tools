import React from 'react'
import { toPersianNumber } from '../services/lang'
import useRequest from '../Api/use-request'
import { useAppState } from '../state/useAppState'
import { v4 as uuidv4 } from 'uuid'

function CommentCard({ index, commentId, userId, postId, userPhoto, title, likeId, unlikeId, text }) {
    const { dispatch } = useAppState()
    const [likeMode, setLikeMode] = React.useState("updated")
    const [unlikeMode, setUnlikeMode] = React.useState("updated")

    // TODO: METHOD ==> POST create like/unlike schema and update state
    const { doRequest: like } = useRequest({
        url: `/likes`,
        method: 'post',
        body: {
            id: uuidv4(),
            userId,
            postId,
            commentId
        },
        onSuccess: (data) => {
          if(data) {
            // TODO: update state
            dispatch({ type: 'UPDATE_LIKES', payload: { index, data } })
            setLikeMode("updating")
          }
        }
    })
    const { doRequest: unlike } = useRequest({
        url: `/unlikes`,
        method: 'post',
        body: {
            id: uuidv4(),
            userId,
            postId,
            commentId
        },
        onSuccess: (data) => {
          if(data) {
            // TODO: update state
            dispatch({ type: 'UPDATE_UNLIKES', payload: { index, data} })
            setUnlikeMode("updating")
          }
        }
    })

    // TODO: METHOD ==> PUT  update likeId/unlikeId on comments schema
    const { doRequest: updateLike } = useRequest({
        url: `/comments/${commentId}`,
        method: 'patch',
        body: {
            likeId
        },
        onSuccess: (data) => {
          if(data) {
            setLikeMode("updated")
          }
        }
    })
    const { doRequest: updateUnlike } = useRequest({
        url: `/comments/${commentId}`,
        method: 'patch',
        body: {
            unlikeId
        },
        onSuccess: (data) => {
          if(data) {
            setUnlikeMode("updated")
          }
        }
    })

    const handlingLikes = async () => {
        await like()
    }
    const handlingUnlikes = async () => {
        await unlike()
    }

    React.useEffect(() => {
        if("updating" === likeMode) {
            updateLike()
        }
    }, [likeId, likeMode])

    React.useEffect(() => {
        if("updating" === unlikeMode) {
            updateUnlike()
        }
    }, [unlikeId, unlikeMode])

    return (
        <div className='comment'>
            {/* TODO: head */}
            <div className='comment__head'>
                <div className='comment__head--right'>
                    <img alt='' src={userPhoto} className='comment__user-photo' />
                    <span className='comment__title'>{title}</span>
                </div>
                <div className='comment__head--left'>
                    <div className='comment__createdAt'>
                        <div className='comment__createdAt--content'>
                            <span className='comment__createdAt--text'>ساعت : </span>
                            <span className='comment__createdAt--time'>{toPersianNumber('16:48')}</span>
                        </div>
                        <div className='comment__createdAt--line'></div>
                        <div className='comment__createdAt--content'>
                            <span className='comment__createdAt--text'>تاریخ : </span>
                            <span className='comment__createdAt--time'>{toPersianNumber('1400/2/15')}</span>
                        </div>
                    </div>

                    <div className='comment__activity'>
                        <div className='comment__activity-box--right'>
                            <svg className="comment__activity--icon-positive">
                                <use xlinkHref="/SVG/sprite.svg#icon-happy"></use>
                            </svg>
                            <span className='comment__activity--count'>{toPersianNumber(likeId.length)}</span>
                        </div>
                        <div className='comment__activity-box--left'>
                            <svg className="comment__activity--icon-negative">
                                <use xlinkHref="/SVG/sprite.svg#icon-sad"></use>
                            </svg>
                            <span className='comment__activity--count'>{toPersianNumber(unlikeId.length)}</span>
                        </div>
                    </div>
                </div>

            </div>
            {/* TODO: body */}
            <div className='comment__body'>
                <div className='comment__body--column-top'>
                    <span className='comment__comment--text'>
                        {text}
                    </span>
                </div>
                
                <div className='comment__body--column-bottom'>
                    <button className='comment__button' onClick={handlingLikes}>
                        <div className="comment__button--box">
                            <svg className="comment__button--icon-positive ml-10">
                                <use xlinkHref="/SVG/sprite.svg#icon-happy"></use>
                            </svg>
                            <span className="comment__button--text-positive">پاسخ خوب بود</span>
                        </div>
                    </button>
                    <button className='comment__button' onClick={handlingUnlikes}>
                        <div className="comment__button--box">
                            <svg className="comment__button--icon-negative ml-10">
                                <use xlinkHref="/SVG/sprite.svg#icon-sad"></use>
                            </svg>
                            <span className="comment__button--text-negative">پاسخ خوب نبود</span>
                        </div>
                    </button>
                </div>

            </div>
        </div>
    )
}

export default CommentCard