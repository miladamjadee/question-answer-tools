import React from 'react'
import useRequest from '../Api/use-request'
import { v4 as uuidv4 } from 'uuid'
import { useAppState } from '../state/useAppState'

function Form({ postId, userId, commentsId }) {
  const [loading, setLoading] = React.useState(false)
  const [mode, setMode] = React.useState("updated")
  const [comment, setComment] = React.useState("")
  const [buttonText, setButtonText] = React.useState("ارسال پاسخ")
  const { dispatch } = useAppState()
  
  // TODO: METHOD ==> POST create comment schema and update state
  const { doRequest } = useRequest({
    url: `/comments`,
    method: 'post',
    body: {
      id: uuidv4(),
      userId,
      comment,
      postId,
      likeId: [],
      unlikeId: []
    },
    onSuccess: (data) => {
      if(data) {
        setLoading(false)
        setComment("")
        setButtonText("ارسال پاسخ")
        // TODO: update state
        dispatch({ type: 'UPDATE_COMMENT', payload: data })
        setMode("updating")
      }
    }
  })
  // TODO: METHOD ==> PUT  update commentId on posts schema
  const { doRequest: updatedComment } = useRequest({
    url: `/posts/${postId}`,
    method: 'patch',
    body: {
      commentsId
    },
    onSuccess: (data) => {
      if(data) {
        setMode("updated")
      }
    }
  })

  const handlingSubmit = async () => {
    if(comment !== "") {
      setLoading(true)
      setButtonText("در حال ارسال")
      await doRequest()
    }
  }

  React.useEffect(() => {
    if(typeof postId !== "undefined" && "updating" === mode) {
      updatedComment()
    }
  }, [commentsId, mode])
  
  return (
    <form className='form'>
        <span className='heading-primary mt-24 mb-18'>
          <span className='heading-primary--main'>
            پاسخ خود را ثبت کنید
          </span>
        </span>

        <span className='heading-primary mb-10'>
          <span className='heading-primary--sub'>
            پاسخ خود را بنویسید
          </span>
        </span>

        <label aria-label="comment" htmlFor='dat18' className='form__label'>
          <textarea placeholder='متن پاسخ ...' id='dat18' rows="8" name='comment' value={comment} className='form__textarea' onChange={(e) => setComment(e.target.value)}></textarea>
        </label>
        
        <div className='form__error'>
          <span className='form__error--guide'>
            محل قرارگیری متن راهنمای خطا
          </span>
        </div>
        
        <button type="button" className='form__button' onClick={handlingSubmit}>
          <span className='form__button--text'>{buttonText}</span>
        </button>
    </form>
  )
}

export default Form