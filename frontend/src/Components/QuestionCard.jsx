import React from 'react'
import { toPersianNumber } from '../services/lang'
import { useNavigate } from 'react-router-dom'


function QuestionCard({ questionId, userPhoto, title, count, text, hasButton = true, errorPhoto }) {
    
    let navigate = useNavigate()

    const goToPage = () => {
        navigate(`/question/${questionId}`)
    }

    return (
        <div className='question'>
            {/* TODO: head */}
            <div className='question__head'>
                <div className='question__head--right'>
                    <img alt='' src={userPhoto} className='question__user-photo' />
                    <span className='question__title'>{title}</span>
                </div>
                <div className='question__head--left'>
                    <div className='question__createdAt'>
                        <div className='question__createdAt--content'>
                            <span className='question__createdAt--text'>ساعت : </span>
                            <span className='question__createdAt--time'>{toPersianNumber('16:48')}</span>
                        </div>
                        <div className='question__createdAt--line'></div>
                        <div className='question__createdAt--content'>
                            <span className='question__createdAt--text'>تاریخ : </span>
                            <span className='question__createdAt--time'>{toPersianNumber('1400/2/15')}</span>
                        </div>
                    </div>

                    <div className='question__comment'>
                        <svg className="question__comment--icon">
                            <use xlinkHref="/SVG/sprite.svg#icon-bubble"></use>
                        </svg>
                        <span className='question__comment--count'>{toPersianNumber(count)}</span>
                    </div>
                </div>

            </div>
            {/* TODO: body */}
            <div className='question__body'>
                <div className='question__body--column-top'>
                    <span className='question__comment--text'>
                        {text}
                    </span>
                </div>
                {hasButton && (<div className='question__body--column-bottom mt-24'>
                    <button type='button' className='question__button' onClick={goToPage}>
                        <span className='question__button--text'>مشاهده جزئیات</span>
                    </button>
                </div>)}

                {errorPhoto && errorPhoto !== "" && (
                    <div className='question__error'>
                        <img alt='error' src={errorPhoto} className="question__error--photo" />
                    </div>
                )}
            </div>
        </div>
    )
}

export default QuestionCard