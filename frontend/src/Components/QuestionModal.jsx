import React from 'react'
import { useAppState } from '../state/useAppState'
import useRequest from '../Api/use-request'
import { v4 as uuidv4 } from 'uuid'

function QuestionModal() {
    const [loading, setLoading] = React.useState(false)
    const [values, setValues] = React.useState({
        title: '',
        description: ''
    })
    const { state, dispatch } = useAppState()
 
    const { doRequest, errors } = useRequest({
        url: `/posts`,
        method: 'post',
        body: {
            id: uuidv4(),
            userId: state.user.id,
            ...values,
            commentsId: [],
            photo: null,
        },
        onSuccess: (data) => {
          if(data) {
            setLoading(false)
            setValues({ title: '', description: '' })
            data.user = state.user
            dispatch({ type: 'ADD_POST', payload: data })
            dispatch({ type: 'CLOSE_MODAL', payload: false })
          }
        }
    })

    const handlingChange = (e) => {
        setValues({ ...values, [e.target.name]: e.target.value })
    }

    const handlingSubmit = async () => {
        const { title, description } = values
        if(title !== "" && description !== "") {
            setLoading(true)
            await doRequest()
        }
    }

    if (state.isOpenModal) {
        return (
            <div className='modal'>
                <div className='modal__dialog'>
                    {/* TODO: Header */}
                    <div className='modal__header'>
                        <h5 className='modal__title'>ایجاد سوال جدید</h5>
                        <button 
                            aria-label='close' 
                            type='button' 
                            className='modal__close' 
                            onClick={() => dispatch({ type: 'CLOSE_MODAL', payload: false })}
                        >
                            <svg className="modal__close--icon">
                                <use xlinkHref="/SVG/sprite.svg#icon-close"></use>
                            </svg>
                        </button>
                    </div>
                    {/* TODO: Body */}
                    <div className='modal__body'>
                        <form>
                            <div className="form-group">
                                <label htmlFor='title' className="col-form-label mb-10">موضوع</label>
                                <input type="text" name='title' className="form-control hgt-44" value={values.title} id="title" onChange={handlingChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor='desciption' className="col-form-label mb-10">متن سوال</label>
                                <textarea className="form-control resize-none" rows="7" name='description' value={values.description} id="desciption" onChange={handlingChange}></textarea>
                            </div>
                        </form>
                    </div>
                    {/* TODO: Footer */}
                    <div className="modal__footer">
                        <button 
                            type="button" 
                            className="btn btn-secondary"
                            onClick={() => dispatch({ type: 'CLOSE_MODAL', payload: false })}
                        >
                            انصراف
                        </button>
                        <button type="button" onClick={handlingSubmit} className="btn btn-primary">ایجاد سوال</button>
                    </div>
                </div>
            </div>
        )
    }

    return <></>
}

export default QuestionModal