import React from 'react'

function Content(props) {
  return (
    <div className='main'>{props.children}</div>
  )
}

export default Content