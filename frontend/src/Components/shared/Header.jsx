import React from "react";
import { useAppState } from '../../state/useAppState'

function Header({ overview }) {
  const { dispatch } = useAppState()

  return (
    <header className="header">
      <div className="header__container">
        {/* TODO: Overview Heading */}
        <div className="overview">
          <h1 className="overview__heading">{overview}</h1>
        </div>
        {/* TODO: User Nav */}
        <nav className="user-nav">
          <button type="button" className="user-nav__button" onClick={() => dispatch({type: 'OPEN_MODAL', payload: true })}>
            <div className="user-nav__button--box">
              <svg className="user-nav__button--icon">
                <use xlinkHref="/SVG/sprite.svg#icon-add"></use>
              </svg>
              <span className="user-nav__button--text">سوال جدید</span>
            </div>
          </button>
          <div className="user-nav__user">
            <img alt="marzieh_ebrahimi" src="/logo192.png" className="user-nav__user-photo" />
            <span className="user-nav__user-name">مرضیه ابراهیمی</span>
            <svg className="user-nav__arrow--drop-down">
              <use xlinkHref="/SVG/sprite.svg#icon-arrow_drop_down"></use>
            </svg>
          </div>
        </nav>
      </div>
    </header>
  );
}

export default Header;
