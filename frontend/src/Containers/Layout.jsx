import React from 'react'
import Header from '../Components/shared/Header'
import Content from '../Components/shared/Content'
import QuestionModal from '../Components/QuestionModal'

function Layout (props) {
    return (
        <div className='container'>
            <Header overview={props.overview} />
            <Content>{props.children}</Content>
            <QuestionModal />
        </div>
    )
}

export default Layout