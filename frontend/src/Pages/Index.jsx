import React from 'react'
import { useAppState } from '../state/useAppState'
import useRequest from '../Api/use-request'
import Layout from '../Containers/Layout'
import QuestionCard from '../Components/QuestionCard'

function Index() {
  const [status, setStatus] = React.useState('idle')
  const { state: { posts }, dispatch} = useAppState()
  const { doRequest, errors } = useRequest({
    url: `/posts?_expand=user`,
    method: 'get',
    body: {},
    onSuccess: (data) => {
      if(data) {
        setStatus('success')
        dispatch({ type: 'GET_POSTS', payload: data })
      }
    }
  })

  const fetchPosts = async () => {
    await doRequest()
  }

  React.useEffect(() => {
    if('idle' === status) {
      fetchPosts()
    }
  }, [status])

  const renderPosts = posts.map(post => {
    return (
      <QuestionCard
          key={post.id}
          questionId={post.id}
          userPhoto={post.user.avatar}
          title={post.title}
          text={post.description}
          count={post.commentsId.length}
        />
    )
  })
  
  
  return (
    <Layout overview="لیست سوالات">
      <section className='questions'>
        {status === "success" && renderPosts}
      </section>
    </Layout>
  )
}

export default Index