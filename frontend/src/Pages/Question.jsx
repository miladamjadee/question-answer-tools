import React from 'react'
import Layout from '../Containers/Layout'
import QuestionCard from '../Components/QuestionCard'
import CommentCard from '../Components/CommentCard'
import Form from '../Components/Form'
import { useAppState } from '../state/useAppState'
import { useParams } from 'react-router-dom'
import useRequest from '../Api/use-request'


function Question() {
  const [status, setStatus] = React.useState("idle")
  const { state: { post }, dispatch} = useAppState()
  const { slug } = useParams()

  const { doRequest, errors } = useRequest({
    url: `/posts/${slug}?_expand=user&_embed=comments`,
    method: 'get',
    body: {},
    onSuccess: (data) => {
      if(data) {
        setStatus('success')
        dispatch({ type: 'GET_POST', payload: data })
      }
    }
  })

  const fetchPost = async () => {
    await doRequest()
  }

  React.useEffect(() => {
    if('idle' === status) {
      fetchPost()
    }
  }, [status])

  const renderComments = post.comments?.map((comment, index) => {
    return (
      <CommentCard
        key={comment.id}
        index={index}
        commentId={comment.id}
        postId={post.id}
        userId={comment.userId}
        userPhoto="/node.jpg"
        title="سید مصطفی"
        text={comment.comment}
        likeId={comment.likeId}
        unlikeId={comment.unlikeId}
      />
    )
  })

  return (
    <Layout overview="جزئیات سوال">
        {/* TODO: Single Question */}
        {status === "success" && (<QuestionCard
          questionId={post.id}
          userPhoto={post.user.avatar}
          title={post.title}
          text={post.description}
          count={post.commentsId.length}
          hasButton={false}
          errorPhoto={post.photo}
        />)}
        <span className='heading-primary mt-24 mb-16'>
          <span className='heading-primary--main'>
            پاسخ ها
          </span>
        </span>
        {/* TODO: Comments */}
        <section className='comments'>
          {status === "success" && post.comments.length > 0 && renderComments}
        </section>
        {/* TODO: Comment Form */}
        <Form postId={post.id} userId='0d0c3408-a51a-4e24-b3ce-609ce2a0fa1c' commentsId={post.commentsId} />
    </Layout>
  )
}

export default Question