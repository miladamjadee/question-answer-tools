import React from 'react'
import AppContext from './appContext';
import reducer from './reducer';
import initState from './initState';

export default function AppStateProvider({ children }) {
    const [state, dispatch] = React.useReducer(reducer, initState)

    return (
        <AppContext.Provider value={{ state, dispatch }}>
            {children}
        </AppContext.Provider>
    )
}
