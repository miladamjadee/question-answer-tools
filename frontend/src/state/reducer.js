import * as types from './types'
import initState from './initState'

const appReducer = (state = initState, action) => {
    let newState = state
    switch (action.type) {
        case types.INIT_STATE:
            newState = action.payload
            break;
        case types.OPEN_MODAL:
            newState = { ...state, isOpenModal: action.payload }
            break;
        case types.CLOSE_MODAL:
            newState = { ...state, isOpenModal: action.payload }
            break;
        case types.GET_POSTS:
            newState = { ...state, posts: action.payload }
            break;
        case types.GET_POST:
            newState = { ...state, post: action.payload }
            break;
        case types.ADD_POST:
            newState = { ...state, posts: [...state.posts, action.payload ] }
            break;
        case types.UPDATE_COMMENT:
            newState = { ...state, post: { ...state.post, commentsId: [...state.post.commentsId, action.payload.id], comments: [...state.post.comments, action.payload] } }
            break;
        case types.UPDATE_LIKES:
            let nextComments = [...state.post.comments]
            nextComments[action.payload.index] = { ...nextComments[action.payload.index], likeId: [ ...nextComments[action.payload.index].likeId, action.payload.data.id ]}
            
            newState = { ...state, post: { ...state.post,  comments: [ ...nextComments ] } }
            break;
        case types.UPDATE_UNLIKES:
            let newComments = [...state.post.comments]
            newComments[action.payload.index] = { ...newComments[action.payload.index], unlikeId: [ ...newComments[action.payload.index].unlikeId, action.payload.data.id ]}
            
            newState = { ...state, post: { ...state.post,  comments: [ ...newComments ] } }
            break;
    
    }
    return newState
}

export default appReducer