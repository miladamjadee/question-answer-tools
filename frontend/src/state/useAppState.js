import React from 'react'
import AppContext from './appContext'

export const useAppState = () => {
    return React.useContext(AppContext)
}
